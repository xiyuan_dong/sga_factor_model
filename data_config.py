pricing_data_item_dict = {
    'TOTRETLOC': 'TotRetLoc',
    'TOTRETUSD': 'TotRetUSD',
    'AVGVOLUME': 'avgVolume',
    'MCAPLOC': 'MCAPLOC',
    'MCAPUSD': 'MCAPUSD'
}


financial_data_item_dict = {
    'NETINCOME': 15,
    'TOTASSETS': 1007,
    'TOTEQUITY': 1275,
    'SGA': 102,
    'RDEXP': 100
    }

estimates_data_item_dict = {'EPS': 'EPS'}

period_type_dict = {'ANN': 1,
                    'QTR': 2,
                    'YTD': 3,
                    'LTM': 4,
                    'SA': 10,
                    'I': 17}

