import pandas as pd
import inspect


def retrieve_name(var):
    """
    Gets the name of var. Does it from the out most frame inner-wards.
    :param var: variable to get name from.
    :return: string
    """
    for fi_ in reversed(inspect.stack()):
        names = [var_name for var_name, var_val in fi_.frame.f_locals.items() if var_val is var]
        if len(names) > 0:
            return names[0]
    return [None]


def format_for_cols(val: pd.Series):
    """
    add variable names to cols
    :param val:
    :return:
    """
    if isinstance(val, pd.Series):
        val.index = [retrieve_name(val) + '.' + x for x in val.index]
    return val