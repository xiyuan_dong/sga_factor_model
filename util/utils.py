from typing import List

import datetime as dt
import dateutil.relativedelta
import pandas as pd
import numpy as np
import os
import psutil


def calc_score(factor: pd.Series, group: str) -> pd.Series:
    if group == 'default':
        rank = factor.rank(method='average', na_option='keep', ascending=True)/(factor.count(level=None)+1)
    else:
        rank = factor.groupby(group).transform('ranks')/(factor.groupby(group).transform('count')+1)
    return rank.transform(lambda x: (x - x.mean()) / x.std())


def time_diff(dates: dt.date, freq: str, n: int) -> dt.date:
    if freq == 'M':
        return dates + dateutil.relativedelta.relativedelta(months=n)
    if freq == 'Y':
        return dates + dateutil.relativedelta.relativedelta(years=n)
    if freq == 'D':
        return dates + dateutil.relativedelta.relativedelta(days=n)


def quarter_end_date(period_range: int) -> dt.date:
    year = str(period_range//100)
    quarter = period_range % 100
    end_date = {1: '04-30', 2: '07-31', 3: '09-30', 4: '01-31'}
    return dt.datetime.strptime(year + '-' + end_date[quarter], '%Y-%m-%d').date()


def show_memory_info(hint):
    pid = os.getpid()
    p = psutil.Process(pid)

    info = p.memory_full_info()
    memory = info.uss / 1024. / 1024
    print('{} memory used: {} MB'.format(hint, memory))


def mute_low_mcap_securities(df: pd.DataFrame, columns: List[str], limit: float = 0.001,
                             mcap_column: str = 'MCAPUSD') -> pd.DataFrame:
    """
    very specific function to risk risk_factors, makes the stocks data which don't meet MCAPUSD criteria = Nan
    Parameters:
    -----------------------------------------------------------------
    :param columns:
    :param mcap_column:
    :param df
    :param limit
    """
    for col in columns:
        df.loc[df[mcap_column] <= limit, col] = np.nan
    return df


