import pandas as pd
from data import CompanyUniverse


class CalcManager:
    """
    Calculate factors as of each observation date all at once, take filtered data from FilterManager
    """
    def __init__(self, factor_list, filtered_data_date, date, keep_inputs_flag):
        self.factor_list = factor_list
        self.filtered_data_date = filtered_data_date
        self.date = date
        self.keep_inputs_flag = keep_inputs_flag

    def calc(self, fac):
        fac_data = self.get_fac_data(fac)
        fac_data = fac_data.apply(lambda col: pd.to_numeric(col, errors='coerce'))
        fac_instance = fac(fac_data)
        df_fac = fac_instance.compute()[0]
        df_value = fac_instance.compute()[1]
        return df_fac, df_value

    @staticmethod
    def get_fac_data(filtered_data, fac):
        """
        :param filtered_data: a dictinary of cleaned data
        :param fac: factor instance
        :return: fac_data: a data frame of data inputs.
                Two levels of row index: observation date / company Id
                Two levels of column index: data item + frequency / lagged period:
        """
        full_univ = CompanyUniverse.univ.dropna().set_index(['referenceDate', 'companyId']).index
        list_of_df = []
        for col in fac.data_cols:
            col_data = filtered_data[col.data_class].loc[:,
                            filtered_data[col.data_class].columns.get_level_values(0).isin([col.name]) &
                            filtered_data[col.data_class].columns.get_level_values(1).isin(col.period_list)]
            if isinstance(col_data, pd.Series):
                col_data = col_data.to_frame()
            list_of_df.append(col_data)
        fac_data = pd.concat(list_of_df, axis=1)
        missing_idx = full_univ[~full_univ.isin(fac_data.index)]
        if len(missing_idx) != 0:
            fac_data = fac_data.reindex(full_univ)
        fac_data = fac_data.astype(float)
        return fac_data

    def calc_all(self):
        """
        :param keep_inputs_flag: If true, also return all data inputs as a data frame for users to check on factor data
        :return: df_value / df_score: data frame of factor value and score
                factor_inputs: a dictionary of factor data inputs, key: factor name, value: data frame of data inputs
        """
        value_result_list = []
        score_result_list = []
        factor_inputs = {}
        for fac in self.factor_list:
            # get data for factor calculation
            fac_data = CalcManager.get_fac_data(filtered_data=self.filtered_data_date, fac=fac)
            # instantiate factor instance
            fac_instance = fac(fac_data)
            if self.keep_inputs_flag:
                factor_inputs[fac.name] = fac_data
            value_result_list.append(fac_instance.compute()[0])
            score_result_list.append(fac_instance.compute()[1])
        df_value = pd.concat(value_result_list, axis=1)
        df_score = pd.concat(score_result_list, axis=1)
        return df_value, df_score, factor_inputs

    @staticmethod
    def calc_single(fac, fac_data):
        # for testing compute methods
        fac_instance = fac(fac_data)
        value, score = fac_instance.compute()
        value.name = value.name + '_value'
        score.name = score.name + '_score'
        return pd.concat([value, score], axis=1)



