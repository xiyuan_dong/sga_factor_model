from sqlalchemy import create_engine
import pandas as pd
import warnings
import numpy as np
warnings.filterwarnings("ignore")

conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
                 'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
sga_research_engine = create_engine(conn_str)

def find_bm(row:pd.Series):
    bm = " ".join(list(row.index.values[row==1]))
    return bm

query = """
    select  r.btdate, r.ENTITY_ID, r.COMPANYNAME, r.sedolchk, r.mcapusd, cr.COUNTRY,
      MSCI_WDXUSIMI_WT, MSCI_US_WT, MSCI_ACWDXUSSM_WT, MSCI_EEM_WT, MSCI_MSEMSM_WT, R3000_WT, MSCI_CHINAALLSHARES_WT, SPBMI_WT
	from
        ( SELECT btdate, SEDOLCHK, MSCI_WDXUSIMI_WT, MSCI_US_WT, MSCI_ACWDXUSSM_WT, MSCI_EEM_WT, MSCI_MSEMSM_WT, R3000_WT, MSCI_CHINAALLSHARES_WT, SPBMI_WT
		FROM riskdb.dbo.SGP_HoldingsDataTable_NEW
		where (msci_wdxusimi_wt>0 or MSCI_US_WT>0 or MSCI_ACWDXUSSM_WT>0 and msci_eem_wt>0 or MSCI_MSEMSM_WT>0  or r3000_wt>0 or MSCI_CHINAALLSHARES_WT>0 or spbmi_wt>0)
		and len(sedolchk)=7 and left(Sedolchk,2) <> 'TS') h
      join
      riskdb.dbo.SGP_ResearchDataTable_NEW r
      on h.sedolchk = r.sedolchk and r.btdate = h.btdate
       left join
	  [RiskDB].[dbo].[SGP_CountryRegionMap] cr
	  on r.COUNTRY = cr.COUNTRYN
	  left join
      ResearchDB.dbo.[ciqSecurityIdentifier] sid
      on h.sedolchk = sid.identifiervalue
      left join
      ResearchDB.dbo.[ciqTradingItemIdentifier] tid
      on h.sedolchk = tid.identifiervalue
      where sid.securityId is null and tid.[tradingItemId] is null
order  by btdate desc , mcapusd desc
"""
identifiers = pd.read_sql(query, sga_research_engine)
id = identifiers.groupby('sedolchk').agg({'btdate': {'fromDate': np.min, 'toDate': np.max},
                                          'mcapusd':{'max_mcap_usd': np.max}})
id.columns = id.columns.droplevel()
id.reset_index(inplace=True)
id['sedolchk']= id['sedolchk'].astype(str)
id_name = identifiers.loc[:, ['ENTITY_ID', 'COMPANYNAME', 'sedolchk']].drop_duplicates()
id_name['sedolchk']= id_name['sedolchk'].astype(str)
id = id.merge(id_name, on='sedolchk', how='left')
bm_col = [bm for bm in identifiers.columns if '_WT' in bm]
id_bm = identifiers.loc[:, ['btdate', 'sedolchk'] + bm_col]
id_bm = id_bm.sort_values('btdate',ascending = False).groupby('sedolchk').head(1)
id_bm.drop('btdate', axis=1, inplace=True)
id_bm.reset_index(drop=True, inplace=True)
id_bm.loc[:, bm_col] = id_bm.loc[:, bm_col].mask(id_bm.loc[:, bm_col]!= 0, 1)
id_bm.columns.values[1:] = [i.rstrip('_WT') for i in bm_col]

id_bm['Benchmark'] = id_bm.apply(find_bm, axis = 1)
id_bm.drop([i.rstrip('_WT') for i in bm_col], axis=1, inplace=True)
id = id.merge(id_bm, on='sedolchk', how='left')
id = id.loc[:, ['sedolchk', 'ENTITY_ID', 'COMPANYNAME', 'Benchmark', 'fromDate', 'toDate', 'max_mcap_usd']]


query = """

      select  r.btdate, r.ENTITY_ID, r.COMPANYNAME, r.sedolchk, r.mcapusd,cr.COUNTRY,
      MSCI_WDXUSIMI_WT, MSCI_US_WT, MSCI_ACWDXUSSM_WT, MSCI_EEM_WT, MSCI_MSEMSM_WT, R3000_WT, MSCI_CHINAALLSHARES_WT, SPBMI_WT
	  from
      ( SELECT btdate, SEDOLCHK, MSCI_WDXUSIMI_WT, MSCI_US_WT, MSCI_ACWDXUSSM_WT, MSCI_EEM_WT, MSCI_MSEMSM_WT, R3000_WT, MSCI_CHINAALLSHARES_WT, SPBMI_WT
		FROM riskdb.dbo.SGP_HoldingsDataTable_NEW
		where (msci_wdxusimi_wt>0 or MSCI_US_WT>0 or MSCI_ACWDXUSSM_WT>0 and msci_eem_wt>0 or MSCI_MSEMSM_WT>0  or r3000_wt>0 or MSCI_CHINAALLSHARES_WT>0 or spbmi_wt>0)
		and len(sedolchk)=7 and left(Sedolchk,2) <> 'TS') h
      join
      riskdb.dbo.SGP_ResearchDataTable_NEW r
      on h.sedolchk = r.sedolchk and r.btdate = h.btdate
      left join
	  [RiskDB].[dbo].[SGP_CountryRegionMap] cr
	  on r.COUNTRY = cr.COUNTRYN
	  left join
      (select * from xf_target.dbo.ciqsymbol where symboltypeid in (13,5700)) sym1
      on h.sedolchk = sym1.symbolValue
      left join
      (select * from xf_target.dbo.ciqsymbol where symboltypeid in (21,5701)) sym2
      on h.sedolchk = sym2.symbolValue
      where sym1.objectId IS NULL AND sym2.objectId IS NULL
order  by btdate desc , mcapusd desc
"""
symbols = pd.read_sql(query, sga_research_engine)
sym = symbols.groupby('sedolchk').agg({'btdate': {'fromDate': np.min, 'toDate': np.max},
                                          'mcapusd':{'max_mcap_usd': np.max}})
sym.columns = sym.columns.droplevel()
sym.reset_index(inplace=True)
sym['sedolchk']= sym['sedolchk'].astype(str)
sym_name = symbols.loc[:, ['ENTITY_ID', 'COMPANYNAME', 'sedolchk']].drop_duplicates()
sym_name['sedolchk']= sym_name['sedolchk'].astype(str)
sym = sym.merge(sym_name, on='sedolchk', how='left')
bm_col = [bm for bm in symbols.columns if '_WT' in bm]
sym_bm = symbols.loc[:, ['btdate', 'sedolchk'] + bm_col]
sym_bm = sym_bm.sort_values('btdate',ascending = False).groupby('sedolchk').head(1)
sym_bm.drop('btdate', axis=1, inplace=True)
sym_bm.reset_index(drop=True, inplace=True)
sym_bm.loc[:, bm_col] = sym_bm.loc[:, bm_col].mask(sym_bm.loc[:, bm_col]!= 0, 1)
sym_bm.columns.values[1:] = [i.rstrip('_WT') for i in bm_col]

sym_bm['Benchmark'] = sym_bm.apply(find_bm, axis = 1)
sym_bm.drop([i.rstrip('_WT') for i in bm_col], axis=1, inplace=True)
sym = sym.merge(sym_bm, on='sedolchk', how='left')
sym = sym.loc[:, ['sedolchk', 'ENTITY_ID', 'COMPANYNAME', 'Benchmark', 'fromDate', 'toDate', 'max_mcap_usd']]

sym_id = id.merge(sym.loc[:, ['sedolchk']], how='inner', on='sedolchk')


excel_file_name = 'sedol_coverage.xlsx'
writer = pd.ExcelWriter(excel_file_name)
sym_id.to_excel(writer, 'NotInBoth')
id.to_excel(writer, 'NotInCiqIdentifiers')
sym.to_excel(writer, 'NotInCiqSymbol')
writer.close()