from filter import FilterManager
from model import ModelManager
from calc import CalcManager
from demo.GLBET import GLBET

import warnings

import pandas as pd
from sqlalchemy import create_engine

warnings.filterwarnings("ignore")


start1 = '2020-01-31'
end1 = '2020-08-31'
m = ModelManager(universe=['ALL'], factor_list=[GLBET], start_date=start1, end_date=end1)
m.import_data(pd.to_datetime('2020-01-31'))

glbet_data = m.check_fac_data_inputs(dt=pd.to_datetime('2020-01-31'), fac=GLBET)

from data import CIQEstimatesData

f = FilterManager(universe=m.universe, factor_list=m.factor_list, date=m.date_list[0],
                       financial=m.financial, pricing=m.pricing, estimates=m.estimates_cols,
                         data_class_list=m.data_class_list)

test = f.filter_all()

c = CalcManager(factor_list=f.factor_list, filtered_data_date=test, date=f.date)
check = c.get_fac_data(fac=c.factor_list[0])


edw = f.estimates[4]
edw.cal_statistics(raw_data=CIQEstimatesData.data)


def run_model(factor):
    start1 = '2020-01-31'
    end1 = '2020-08-31'
    m = ModelManager(universe=['ALL'], factor_list=[EREVFY1FY2], start_date=start1, end_date=end1)
    m.set_data()
    m.run_filter()
    m.run_model()
    value = m.values
    factor = m.factor
    value = value.append(m.values, ignore_index=False)
    factor = factor.append(m.factor, ignore_index=False)
    value.to_csv('GLVOL_value.csv')
    factor.to_csv('GLVOL_factor.csv')
    df = pd.merge(factor, value, left_index=True, right_index=True, suffixes=("_factor", "_value"))
    del value, factor
    return df


def compare():
    df = run_model('GLVOL')
    query = """
    SELECT r.BTDATE2 as referenceDate, COALESCE(sec2.companyId, sec1.companyId, symbol2.companyId, symbol1.companyId) as companyId, r.VOLATILITY, fe.GLVOL
FROM
(select BTDATE, EOMONTH(CONVERT(date, (left( BTDATE, 6) + '01'))) AS BTDATE2 ,[SEDOLCHK] ,[VOLATILITY] FROM [RiskDB].[dbo].[SGP_ResearchDataTable_NEW]) r
left join [RiskDB].[dbo].[SGP_FactorExposureDataTable_New] fe
	on r.BTDATE = fe.BTDATE and r.SEDOLCHK = fe.SEDOLCHK
left join (select relatedCompanyId as companyId, symbolValue, isnull(symbolStartDate, '1900-01-01') symbolStartDate, isnull(symbolEndDate, '2099-12-31') symbolEndDate from xf_target.dbo.ciqSymbol sym
                    where symbolTypeId in (13, 5700)) symbol1					 
	on symbol1.symbolValue = r.SEDOLCHK and (r.BTDATE2 between symbol1.symbolStartDate and symbol1.symbolEndDate)
left join (select relatedCompanyId as companyId, symbolValue, isnull(symbolStartDate, '1900-01-01') symbolStartDate, isnull(symbolEndDate, '2099-12-31') symbolEndDate from xf_target.dbo.ciqSymbol sym
                    where symbolTypeId in (22, 5701)) symbol2
	on symbol2.symbolValue = r.SEDOLCHK and (r.BTDATE2 between symbol2.symbolStartDate and symbol2.symbolEndDate)
left join ResearchDB.dbo.[ciqSecurityIdentifier] sid
    on r.sedolchk = sid.identifiervalue and (r.BTDATE2 between isnull(sid.identifierStartDate, '1900-01-01') and ISNULL(sid.identifierEndDate, '2099-12-31') )
left join ResearchDB.dbo.[ciqTradingItemIdentifier] tid
    on r.sedolchk = tid.identifiervalue and (r.BTDATE2 between isnull(tid.identifierStartDate, '1900-01-01') and ISNULL(tid.identifierEndDate, '2099-12-31') )
left join xf_target.dbo.ciqSecurity sec1
	  on sec1.securityId = sid.securityId
left join xf_target.dbo.ciqTradingItem ti
	  on ti.tradingItemId = tid.tradingItemId
left join xf_target.dbo.ciqSecurity sec2
	  on sec2.securityId = ti.securityId
where 1=1
	and r.BTDATE>='19900131' and r.BTDATE<='20200831'
	and len(r.SEDOLCHK)=7 and left(r.SEDOLCHK,2) <> 'TS'
	and COALESCE(sec2.companyId, sec1.companyId, symbol2.companyId, symbol1.companyId) is not null
    """
    conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
               'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
    sga_research_engine = create_engine(conn_str)
    fc = pd.read_sql(query, con=sga_research_engine)
    value = pd.read_csv('GLVOL_value.csv')
    factor = pd.read_csv('GLVOL_factor.csv')
    df = pd.merge(factor, value, on=['referenceDate', 'companyId'], suffixes=("_factor", "_value"))
    del value, factor
    fc.referenceDate = pd.to_datetime(fc.referenceDate)
    df.referenceDate = pd.to_datetime(df.referenceDate)
    cp = pd.merge(fc, df, on=['referenceDate', 'companyId'], how='left')
    cp.GLVOL_value = cp.GLVOL_value*10000
    corr_fac = cp.groupby('referenceDate')[['GLVOL_factor', 'GLVOL']].corr().iloc[0::2, -1]
    corr_vol = cp.groupby('referenceDate')[['GLVOL_value', 'VOLATILITY']].corr().iloc[0::2, -1]
