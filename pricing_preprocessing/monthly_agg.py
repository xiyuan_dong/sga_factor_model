from sqlalchemy import create_engine
from pricing_preprocessing.daily_price import write_df_to_sql
from datetime import datetime
import pandas as pd
import warnings
import numpy as np
import time
warnings.filterwarnings("ignore")


def create_return_monthly(start_idx, end_idx):
    # todo: Update this
    # todo: By month

    print('Processing Companies from {} to {}: ...'.format(start_idx, end_idx))

    # Pulling pricing data from database
    start = time.time()
    query = """
     select pd.pricingDate, EOMONTH(pd.pricingDate) as monthEndDate, pd.companyId, pd.tradingItemId, pd.divAdjPrice, 
     pd.divAdjPrice/exrt.priceClose as divAdjPriceUSD, pd.volume, mcap.marketCap as MCAP,
     mcap.marketCap/exrt.priceClose as MCAPUSD
    from
    (select [companyId] from [ResearchDB].[dbo].CIQ_Company_Univ_unique where idx>={start_idx} and idx<={end_idx}) univ
    join ResearchDB.dbo.CIQ_Pricing_Daily pd on pd.companyId = univ.companyId
    left join [xf_target].[dbo].[ciqMarketCap] mcap 
        on (pd.companyId = mcap.companyId) and (pd.pricingDate = mcap.pricingDate)
    left join [xf_target].[dbo].[ciqExchangeRate] exrt 
        on exrt.currencyId = isnull(pd.currencyId,160) and exrt.priceDate = pd.pricingDate and exrt.snapId = 6
    """.format(start_idx=start_idx, end_idx=end_idx)
    price_d = pd.read_sql(query, sga_research_engine)
    print('*** Pulling data takes: {}s'.format(time.time() - start))

    # Monthly Aggregation

    start2 = time.time()
    # sort pricing table by pricingDate and tradingItemId first to do diff(1)
    price_d.sort_values(by=['tradingItemId', 'pricingDate'], ascending=True, inplace=True)
    price_d = price_d.assign(logRet=np.log(price_d.divAdjPrice).groupby(price_d.tradingItemId).diff(),
                             logRetUSD=np.log(price_d.divAdjPriceUSD).groupby(price_d.tradingItemId).diff())

    # when volume is 0, that price is forward filled, will not be used in calculating variance of daily log return
    # and avg volume

    price_d.loc[price_d["volume"] == 0, ['volume', 'logRet', 'logRetUSD']] = np.nan
    dfgp = price_d.groupby(['monthEndDate', 'tradingItemId'])

    # monthly total return in local currency
    tot = np.exp(dfgp["logRet"].sum()) - 1

    # monthly total return in USD
    tot_usd = np.exp(dfgp["logRetUSD"].sum()) - 1

    # variance of log return
    var = dfgp["logRet"].var()

    # average volumes
    vol = dfgp["volume"].mean()

    # market cap at month end
    mcap = price_d.loc[price_d['pricingDate'] == price_d['monthEndDate'], ['monthEndDate', 'tradingItemId', 'MCAP']].\
        set_index(['monthEndDate', 'tradingItemId']).iloc[:, 0]
    mcap_usd = price_d.loc[price_d['pricingDate'] == price_d['monthEndDate'],
                           ['monthEndDate', 'tradingItemId', 'MCAPUSD']].\
        set_index(['monthEndDate', 'tradingItemId']).iloc[:, 0]

    return_m = pd.concat([tot, tot_usd, var, vol, mcap, mcap_usd], axis=1).reset_index()
    return_m.columns = ['pricingDate', 'tradingItemId', 'TotRetLoc', 'TotRetUSD', 'varLogRet', 'avgVolume', 'MCAPLOC',
                        'MCAPUSD']

    comps = price_d[['pricingDate', 'tradingItemId', 'companyId']]
    comps['pricingDate'] = pd.to_datetime(comps['pricingDate'])
    return_m = pd.merge(return_m, comps, how='inner',
                        on=['pricingDate', 'tradingItemId'])
    # specify data types
    return_m = return_m.astype({'tradingItemId': 'int',
                                'companyId': 'int',
                                'TotRetLoc': 'float',
                                'TotRetUSD': 'float',
                                'varLogRet': 'float',
                                'avgVolume': 'float',
                                'MCAPLOC': 'float',
                                'MCAPUSD': 'float'})
    return_m = return_m[['pricingDate', 'tradingItemId', 'companyId', 'TotRetLoc', 'TotRetUSD', 'MCAPLOC', 'MCAPUSD',
                         'varLogRet', 'avgVolume']]

    # round float number
    return_m[['TotRetLoc', 'TotRetUSD', 'MCAPLOC', 'MCAPUSD', 'varLogRet', 'avgVolume']] = \
        return_m[['TotRetLoc', 'TotRetUSD', 'MCAPLOC', 'MCAPUSD', 'varLogRet', 'avgVolume']].round(6)

    # delete return in 1990-01-31, no data point earlier than 1990
    return_m.replace([np.inf, -np.inf], np.nan, inplace=True)
    return_m.loc[return_m["pricingDate"] == datetime.strptime('1990-01-31', '%Y-%m-%d'), ['TotRetLoc', 'TotRetUSD']] = np.nan

    print('*** Aggregating takes: {}s'.format(time.time() - start2))

    # write data to sql
    start2 = time.time()
    if return_m.shape[0] / 10 ** 6 <= 2:
        return_m.to_sql(name='CIQ_Return_Monthly', con=sga_research_engine, index=False, if_exists='append')
    else:
        write_df_to_sql(df=return_m, name='CIQ_Return_Monthly', con=sga_research_engine, index=False, if_exists='append')
    print('*** Inserting data takes: {}s'.format(time.time() - start2))

    print('Finished processing companies from {} to {}, total takes: {}s'.format(start_idx, end_idx,
                                                                                 time.time() - start))


if __name__ == "__main__":

    conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
                 'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
    sga_research_engine = create_engine(conn_str, fast_executemany=True)
    for i in range(40, 71):
        start_idx = 1000*i+1
        end_idx = 1000*(i+1)
        create_return_monthly(start_idx, end_idx)
