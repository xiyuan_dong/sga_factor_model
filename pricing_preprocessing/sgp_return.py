from sqlalchemy import create_engine
from ..computation.numerical import grouped_weighted_mean
import pandas as pd
import warnings
import numpy as np
import time
warnings.filterwarnings("ignore")


def create_historical_world_index_returns():

    start = time.time()
    query =  """
    select ret.pricingDate, univ.companyId, ret.MCAPUSD, ret.TotRetUSD
    from
    [ResearchDB].[dbo].[CIQ_Company_Universe] univ
    join [ResearchDB].[dbo].[CIQ_Return_Monthly] ret
    on ret.companyId = univ.companyId and ret.pricingDate = univ.referenceDate
    where univ.CALCUNIV=1 and ret.TotRetUSD>=-1 and ret.TotRetUSD<=1 and ret.companyId not in ('SGPMKT')
    """
    data = pd.read_sql(query, sga_research_engine)
    print('*** Pulling data takes: {}s'.format(time.time() - start))
    sgp_mkt = data.groupby(['pricingDate']).apply(grouped_weighted_mean, data_col='TotRetUSD',
                                                    weight='MCAPUSD').reset_index(name='TotRetUSD')
    sgp_mkt['companyId'] = 'SGPMKT'
    sgp_mkt = sgp_mkt.reindex(columns=[*sgp_mkt.columns.tolist(), 'TotRetLoc', 'MCAPLOC', 'MCAPUSD', 'varLogRet',
                                       'aggVolume'], fill_value=np.nan)
    sgp_mkt = sgp_mkt[['pricingDate', 'companyId', 'TotRetLoc', 'TotRetUSD', 'MCAPLOC', 'MCAPUSD',
                         'varLogRet', 'aggVolume']]
    sgp_mkt['TotRetUSD'] = sgp_mkt['TotRetUSD'].round(6)
    sgp_mkt.to_sql(name='CIQ_Return_Monthly', con=sga_research_engine, index=False, if_exists='append')


if __name__ == "__main__":

    conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
                 'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
    sga_research_engine = create_engine(conn_str, fast_executemany=True)

