from sqlalchemy import create_engine
from datetime import datetime
import pandas as pd
import warnings
import numpy as np
import time
warnings.filterwarnings("ignore")


def expand_dates(ser):
    return pd.DataFrame({'pricingDate': pd.date_range(ser['pricingDate'].min(), D2, freq='D')})


def check_duplicates(price_t, price_c):
    duplicates = price_t[price_t.index.duplicated(keep=False)]
    if duplicates.empty:
        return price_t, price_c
    else:
        duplicated_ti = duplicates.index.unique(level='tradingItemId')
        price_t.drop(duplicated_ti, level = 'tradingItemId', inplace=True)
        price_c.drop(duplicated_ti, level='tradingItemId', inplace=True)
        for ti in duplicated_ti.values:
            txt_file.write(str(ti)+'\n')
        return price_t, price_c


def write_df_to_sql(df, chunk_size=6, **kwargs):
    chunks = np.array_split(df, df.shape[0] / 10**chunk_size)
    for chunk in chunks:
        chunk.to_sql(**kwargs)
    return True


def create_pricing_daily(start_idx, end_idx):
    print('Processing Companies from {} to {}: ...'.format(start_idx, end_idx))

    # Pulling pricing data from database
    start = time.time()
    query = """
    select convert(date, pe.pricingDate) as pricingDate, univ.companyId, pe.tradingItemId, ti.currencyId, pe.priceClose, pe.volume, pe.adjustmentFactor, (pe.priceClose*isnull(daf.divAdjFactor,1)) divAdjPrice from
    (SELECT [companyId] FROM [ResearchDB].[dbo].CIQ_Company_Univ_unique where idx>={} and idx<={}) univ
    join xf_target.dbo.ciqSecurity sec on sec.companyId = univ.companyId
    join xf_target.dbo.ciqTradingItem ti on sec.securityId = ti.securityId
    join xf_target.dbo.ciqPriceEquity pe on pe.tradingItemId = ti.tradingItemId
    left join xf_target.dbo.ciqPriceEquityDivAdjFactor daf on pe.tradingItemId = daf.tradingItemId and pe.pricingDate between isnull(daf.fromDate, '1900-01-01') and isnull(daf.toDate, '2099-12-31')
    where 1=1
        and sec.primaryFlag=1 and ti.primaryFlag =1
        and pe.pricingDate>='{}' and pe.pricingDate<='{}'
    """.format(start_idx, end_idx, START_DATE, END_DATE)
    price_t = pd.read_sql(query, sga_research_engine)
    print('*** Pulling data takes: {}s'.format(time.time()-start))

    # Cleaning data and forward filling

    start2 = time.time()
    price_t.pricingDate = pd.to_datetime(price_t.pricingDate, format='%Y-%m-%d')
    price_t.volume = price_t.volume.fillna(value='')
    # columns to be forward filled
    ff_cols = ['companyId', 'currencyId', 'priceClose', 'adjustmentFactor', 'divAdjPrice']

    price_c = price_t.groupby(['tradingItemId']).apply(expand_dates).reset_index() \
        .merge(price_t, how='left')[['pricingDate', 'tradingItemId'] + ff_cols].ffill(limit=FF_LIMIT)

    price_c.dropna(axis=0, how='all',
                   subset=['companyId', 'currencyId', 'priceClose', 'adjustmentFactor', 'divAdjPrice'],
                   inplace=True)
    price_c['volume'] = np.nan

    price_c = price_c.set_index(['pricingDate', 'tradingItemId'])
    price_t = price_t.set_index(['pricingDate', 'tradingItemId'])

    # check if there is any duplicates and write it down (caused by overlapping dividends adjustment factors)
    price_t, price_c = check_duplicates(price_t, price_c)
    # filled weekends/holidays volume with 0, avoid messing up with original NULL value from ciqPriceEquity
    price_c.update(price_t[['volume']])
    price_c = price_c.reset_index()
    price_c.volume = price_c.volume.fillna(value=0)
    price_c.volume = price_c.volume.replace({'': np.nan})
    price_c.pricingDate = price_c.pricingDate.dt.date
    # specify data types
    price_c = price_c.astype({'tradingItemId': 'int',
                              'companyId': 'int',
                              'currencyId': 'Int64',
                              'volume': 'Int64'})
    print('*** Cleaning data and forward filling takes: {}s'.format(time.time() - start2))


    # write data to sql
    start2 = time.time()
    if price_c.shape[0] / 10 ** 6 <=1:
        price_c.to_sql(name='CIQ_Pricing_Daily', con=sga_research_engine, index=False, if_exists='append')
    else:
        write_df_to_sql(df=price_c, name='CIQ_Pricing_Daily', con=sga_research_engine, index=False, if_exists='append')
    print('*** Inserting data takes: {}s'.format(time.time() - start2))

    print('Finished processing companies from {} to {}, total takes: {}s'.format(start_idx, end_idx,
                                                                                 time.time() - start))


if __name__ == "__main__":

    conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
                 'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
    sga_research_engine = create_engine(conn_str, fast_executemany=True)
    START_DATE = '1986-01-01'
    END_DATE = '2020-09-30'
    FF_LIMIT = 30
    D1 = datetime.strptime(START_DATE, '%Y-%m-%d').date()
    D2 = datetime.strptime(END_DATE, '%Y-%m-%d').date()
    txt_file = open('../duplicated_ti5.txt', 'w')
    for i in range(71):
        start_idx = 1000*i+1
        end_idx = 1000*(i+1)
        create_pricing_daily(start_idx, end_idx)
    txt_file.close()

