from data_config import pricing_data_item_dict, financial_data_item_dict
from utils import quarter_end_date
from data import CIQFinancialData, CIQPricingData, CIQEstimatesData, CompanyUniverse, CIQExchangeRate, CurrencyMap
import warnings
import pandas as pd
import numpy as np
pd.options.mode.chained_assignment = None
warnings.simplefilter(action='ignore', category=FutureWarning)


class FilterManager:
    """
    Filter and pre-process required data for each date
    """
    def __init__(self, universe, factor_list, date, financial, pricing, estimates, data_class_list):
        self.universe = universe
        self.factor_list = factor_list
        self.date = date
        self.financial = financial
        self.pricing = pricing
        self.estimates = estimates
        self.data_class_list = data_class_list
        self.price_cy = None

    @staticmethod
    def filter_companies(date):
        """
        :param date:Given a specific date, filter the companies in the universe
        :return: companies
        """
        """"
        # filter companies for research as of each reference date
        """
        return CompanyUniverse.univ.loc[CompanyUniverse.univ.referenceDate == date, :]

    def filter_financial_ciq(self):
        """
        filter the specific financial data for risk_factor calculation
        """
        ciq_fin = {'LTM': CIQFinancialData.ltm_data,
                   'QTR': CIQFinancialData.qtr_data,
                   'ANN': CIQFinancialData.ann_data,
                   'SA': CIQFinancialData.sa_data}
        list_of_df = []
        for period_type in self.financial.keys():
            temp = pd.DataFrame()
            for key, value in self.financial[period_type].items():
                temp = pd.concat([temp, ciq_fin[period_type].loc[
                                        (ciq_fin[period_type].dataItemId == financial_data_item_dict[key])
                                        & (ciq_fin[period_type].N.isin(value))
                                        & (ciq_fin[period_type].referenceDate == self.date), :]], ignore_index=True)

            # filter out the data when the first filing date of this financial period < reference date - (N + 1) *freq
            # e.g. set reference date on 2020-12-31, frequency to annual data, for some reason, company A hasn't
            #       reported for fiscal year 2019, the most recent completed fiscal year of company A we have in the
            #       data base was fiscal year 2018, the first filing date for fiscal year 2018 is 2019-04-01, then
            #       company A will be filtered out for not having updated data.
            # todo: Methodology to be further confirmed
            time_delta_dict = {
                'LTM': 92,
                'QTR': 92,
                'ANN': 365,
                'SA': 183,
            }
            temp = temp.loc[pd.to_datetime(temp.firstFilingDate) >= pd.to_datetime(
                temp.referenceDate) + pd.to_timedelta((temp.N-2) * time_delta_dict[period_type], unit='d'), :]
            temp.drop(columns=['firstFilingDate'], inplace=True)
            temp.reset_index(drop=True, inplace=True)

            # convert currency when companies changed reporting currency period from period
            # todo: Update the time-dependent primary reporting currency table, along with primary trading currency.
            # Ignore this issue for the time being assuming there is no reporting currency change from period to period
            # filtered_financial[period_type] = self.currency_converter(self.currency_map,
            #                                                          filtered_financial[period_type], 'financial')

            temp.drop(columns=['periodRange', 'currencyId'], inplace=True)
            id_to_name = {value: key for key, value in financial_data_item_dict.items()}
            temp['dataItem'] = temp['dataItemId'].map(id_to_name) + '_' + period_type
            # a temporary measure to make sure no duplicates before pivoting, will delete after solving currency issue
            temp.drop_duplicates(subset=['referenceDate', 'companyId', 'dataItem', 'N'], inplace=True)
            temp = pd.pivot(temp, values='dataItemValue', index=['referenceDate', 'companyId'],
                            columns=['dataItem', 'N'])
            list_of_df.append(temp)
        filtered_financial = pd.concat(list_of_df, axis=1)
        return filtered_financial

    def filter_pricing_ciq(self):
        """
        filter the specific pricing data for risk_factor calculation
        """
        # daily price / monthly return need to be filtered differently
        list_of_df = []
        ciq_pricing_func = {'M': self.filter_pricing_ciq_monthly,
                            'D': self.filter_pricing_ciq_daily}
        for period_type in self.pricing.keys():
            list_of_df.append(ciq_pricing_func[period_type](self.pricing[period_type]))
        filtered_pricing = pd.concat(list_of_df, axis=1)
        return filtered_pricing

    def filter_estimates_ciq(self):
        """
            filter the specific estimates data for risk_factor calculation
        """

        filtered_estimates = pd.DataFrame()
        for edw in self.estimates:
            filtered_estimates = filtered_estimates.append(edw.cal_statistics(raw_data=CIQEstimatesData.data))
        filtered_estimates.reset_index(inplace=True)
        filtered_estimates = pd.pivot(filtered_estimates, values='dataItemValue', index=['referenceDate', 'companyId'],
                                      columns=['dataItem', 'N'])
        return filtered_estimates

    def filter_all(self):
        filter_func = {'CAPIQ_Financial': self.filter_financial_ciq,
                       'CAPIQ_Pricing': self.filter_pricing_ciq,
                       'CAPIQ_Estimates': self.filter_estimates_ciq}
        data_filtered = {}
        for data_obj in self.data_class_list:
            data_filtered[data_obj] = filter_func[data_obj]()
        return data_filtered

    @staticmethod
    def currency_converter(currency_map, raw_data, data_type):
        # todo: TO BE MODIFIED
        # 2021-01-13 with David:
        # financials converting to its current fiscal period reporting currency, using the fx rate on period end date
        # pricing convert to financial reporting currency if necessary, using the date of pricing date
        # estimates convert to financial reporting currency if necessary

        if data_type == 'financial':
            quarter_end_date_vec = np.vectorize(quarter_end_date)
            raw_data['date'] = quarter_end_date_vec(raw_data['periodRange'].values)
            merged = raw_data.merge(currency_map, on=['referenceDate', 'companyId'], how='left', suffixes=('_o', '_l'))
            to_be_converted = merged[merged['currencyId_o'] != merged['currencyId_l']]
            # when we don't know the reported currency of a company, drop such records
            is_na_idx = to_be_converted[to_be_converted['currencyId_l'].isnull()].index
            to_be_converted.dropna(subset=['currencyId_l'], inplace=True)
            to_be_converted_idx = to_be_converted.index
            to_be_converted = to_be_converted.merge(CIQExchangeRate.monthly_data, how='left',
                                                    left_on=['date', 'currencyId_o'],
                                                    right_on=['priceDate', 'currencyId'])
            to_be_converted = to_be_converted.merge(CIQExchangeRate.monthly_data, how='left',
                                                    left_on=['date', 'currencyId_l'],
                                                    right_on=['priceDate', 'currencyId'])
            to_be_converted['dataItemValue'] = to_be_converted['dataItemValue'] / to_be_converted['priceClose_x'].\
                astype('float') * to_be_converted['priceClose_y'].astype('float')
            to_be_converted.set_index(to_be_converted_idx, inplace=True)
            raw_data.loc[to_be_converted_idx, 'dataItemValue'] = to_be_converted['dataItemValue']
            raw_data.drop(is_na_idx, inplace=True)
            raw_data.reset_index(drop=True, inplace=True)
            raw_data.drop(columns=['date'], inplace=True)
            return raw_data

        if data_type == 'pricing_monthly':
            merged = raw_data.merge(currency_map, on=['referenceDate', 'companyId'], how='left', suffixes=('_p', '_l'))
            # priceClose to priceLocal
            close_to_local = merged[merged['currencyId_p'] != merged['currencyId_l']]
            # when the current currency is unknown, assuming it aligns with financial report currency
            is_na_idx = close_to_local[close_to_local['currencyId_l'].isnull()].index
            close_to_local_idx = close_to_local.index.drop(is_na_idx)
            close_to_local.dropna(subset=['currencyId_l'], inplace=True)
            close_to_local = close_to_local.merge(CIQExchangeRate.monthly_data, how='left',
                                                  left_on=['monthEndDate', 'currencyId_p'],
                                                  right_on=['priceDate', 'currencyId'])
            close_to_local = close_to_local.merge(CIQExchangeRate.monthly_data, how='left',
                                                  left_on=['monthEndDate', 'currencyId_l'],
                                                  right_on=['priceDate', 'currencyId'])
            close_to_local['local'] = close_to_local['priceClose_x'] / close_to_local['priceClose_y']. \
                astype('float') * close_to_local['priceClose'].astype('float')
            close_to_local.set_index(close_to_local_idx, inplace=True)
            raw_data['local'] = raw_data['priceClose']
            raw_data.loc[close_to_local_idx, 'local'] = close_to_local['local']

            # priceClose to priceUSD
            close_to_usd = merged[merged['currencyId_p'] != 160]
            close_to_usd_idx = close_to_usd.index
            close_to_usd = close_to_usd.merge(CIQExchangeRate.monthly_data, how='left',
                                              left_on=['monthEndDate', 'currencyId_p'],
                                              right_on=['priceDate', 'currencyId'])
            close_to_usd['USD'] = close_to_usd['priceClose_x'] / close_to_usd['priceClose_y'].astype('float')
            close_to_usd.set_index(close_to_usd_idx, inplace=True)
            raw_data['USD'] = raw_data['priceClose']
            raw_data.loc[close_to_usd_idx, 'USD'] = close_to_usd['USD']
            raw_data.rename(columns={'priceClose': 'close'}, inplace=True)
            return raw_data

    @staticmethod
    def filter_pricing_ciq_monthly(item_period_dict):
        filtered_pricing_m = pd.DataFrame()
        for data_item in item_period_dict.keys():
            df = pd.concat([CompanyUniverse.univ.loc[:, ['referenceDate', 'companyId']].assign(N=n)
                            for n in item_period_dict[data_item]], ignore_index=True)
            # df['pricingDate'] = (df.referenceDate.values.astype('M8[M]') + (df.N.values+1) *
            #                     np.timedelta64(1, 'M')).astype('M8[D]') - np.timedelta64(1, 'D')
            if data_item in ['SGPMKT', 'RISKFREE']:
                temp = CIQPricingData.monthly_data.loc[CIQPricingData.monthly_data.companyId == data_item,
                                                       ['referenceDate',  'TotRetUSD', 'N']]
                df = pd.merge(df, temp, how='inner', on=['referenceDate', 'N'])
                df.rename(columns={'TotRetUSD': 'dataIemValue'}, inplace=True)
            else:
                temp = CIQPricingData.monthly_data.loc[~CIQPricingData.monthly_data.companyId.isin(
                    ['SGPMKT', 'RISKFREE']), ['referenceDate', 'companyId', pricing_data_item_dict[data_item], 'N']]
                temp['companyId'] = temp['companyId'].astype(float)
                df = pd.merge(df, temp, how='inner', on=['referenceDate', 'companyId', 'N'])
                df.rename(columns={pricing_data_item_dict[data_item]: 'dataIemValue'}, inplace=True)

            df['dataItem'] = data_item + '_M'
            filtered_pricing_m = pd.concat([filtered_pricing_m, df], ignore_index=True)
        filtered_pricing_m.dropna(subset=['companyId'], inplace=True)
        filtered_pricing_m = filtered_pricing_m.astype({'companyId': 'int'})
        filtered_pricing_m = pd.pivot(filtered_pricing_m, values='dataIemValue', index=['referenceDate', 'companyId'],
                                      columns=['dataItem', 'N'])
        return filtered_pricing_m

    @staticmethod
    def filter_pricing_ciq_daily(item_period_dict):
        filtered_pricing_d = pd.DataFrame()
        pricing_daily_prep_func = {
            'PRICELOC': FilterManager.pricing_daily_price_loc,
            'PRICEUSD': FilterManager.pricing_daily_price_usd,
            'PRICELOC_UNADJ': FilterManager.pricing_daily_price_unadj,
            'VOLUME': FilterManager.pricing_daily_volume,
            'VOLUME_UNADJ': FilterManager.pricing_daily_volume_unadj
        }
        for data_item in item_period_dict.keys():

            df = pd.merge(CompanyUniverse.univ, pricing_daily_prep_func[data_item](), how='inner',
                          on=['referenceDate', 'companyId'])
            df['dataItem'] = data_item + '_D'
            df.rename(columns={data_item: 'dataIemValue'}, inplace=True)

            filtered_pricing_d = pd.concat([filtered_pricing_d, df], ignore_index=True)
        filtered_pricing_d = pd.pivot(filtered_pricing_d, values='dataIemValue', index=['referenceDate', 'companyId'],
                                      columns=['dataItem', 'N'])
        return filtered_pricing_d

    @staticmethod
    def pricing_daily_price_loc():
        return CIQPricingData.daily_data.loc[:, ['referenceDate', 'companyId', 'priceLocal', 'N']].\
            rename(columns={'priceLocal': 'PRICELOC'})

    @staticmethod
    def pricing_daily_price_usd():
        return CIQPricingData.daily_data.loc[:, ['referenceDate', 'companyId', 'priceUSD', 'N']]. \
            rename(columns={'priceUSD': 'PRICEUSD'})

    @staticmethod
    def pricing_daily_price_unadj():
        CIQPricingData.daily_data['PRICELOC_UNADJ'] = CIQPricingData.monthly_data['priceClose'] * \
                                                              CIQPricingData.monthly_data['adjustmentFactor']
        return CIQPricingData.monthly_data.loc[:, ['referenceDate', 'companyId', 'PRICELOC_UNADJ', 'N']]

    @staticmethod
    def pricing_daily_volume():
        return CIQPricingData.daily_data.loc[:, ['referenceDate', 'companyId', 'volume', 'N']].\
            rename({'volume': 'VOLUME'})

    @staticmethod
    def pricing_daily_volume_unadj():
        CIQPricingData.daily_data['VOLUME_UNADJ'] = CIQPricingData.monthly_data['volume'] /\
                                                      CIQPricingData.monthly_data['adjustmentFactor']
        return CIQPricingData.monthly_data.loc[:, ['referenceDate', 'companyId', 'VOLUME_UNADJ', 'N']]





