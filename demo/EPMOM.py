from factor_base import Factor
from data_wrapper import Estimates, Pricing
from sga_python_library.sga_math.sga_math import divide


class EPMOM(Factor):
    """
    Earnings Yield Momentum
    """
    name = 'EPMOM'
    data_cols = [Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='MEDIAN'),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='MEDIAN'),
                 Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='MEDIAN', lag=90),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='MEDIAN', lag=90),
                 Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='NEST'),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='NEST'),
                 Pricing(data_item='PRICELOC', period_type='D', period=0),
                 Pricing(data_item='PRICELOC', period_type='D', period=-90),
                 Pricing(data_item='MCAPUSD', period_type='M', period=0)
                 ]

    def __init__(self, data):
        self.data = data

    def compute(self):
        """
        :return:
        """
        pass