from factor_base import Factor
from data_wrapper import Pricing
from data import CompanyUniverse
import numpy as np
import pandas as pd


class GLBET(Factor):
    """
    World Beta
    """
    name = 'GLBET'
    type = 'RISK'
    data_cols = [Pricing(data_item='TOTRETUSD', period_type='M', period=0, n_period=-60),
                 Pricing(data_item='SGPMKT', period_type='M', period=0, n_period=-60),
                 Pricing(data_item='RISKFREE', period_type='M', period=0, n_period=-60),
                 Pricing(data_item='MCAPUSD', period_type='M', period=0)]

    def __init__(self, data):
        super().__init__()
        self.data = data

    def compute(self):
        # stack company returns
        comp_ret = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'TOTRETUSD_M'].stack(level='N')

        # stack risk-free rate
        rf = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'RISKFREE_M'].stack(level='N')

        # stack Market Return (replicated from SGPMKT from return table calculated from S&P data)
        mkt_ret = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'SGPMKT_M'].stack(level='N')

        mst_ret_table = pd.concat([comp_ret, rf, mkt_ret], axis=1)

        del comp_ret
        del rf
        del mkt_ret

        # get the security which has over 12 returns in over last 60 months
        period_cnt = mst_ret_table.reset_index().groupby('companyId')['TOTRETUSD_M'].count()
        qualified_comp_idx = period_cnt.index[period_cnt >= 12]
        mst_ret_table = mst_ret_table.loc[mst_ret_table.index.isin(qualified_comp_idx, level='companyId'), :]

        del period_cnt
        del qualified_comp_idx

        # calculation
        mst_ret_table['RETURNY'] = mst_ret_table['TOTRETUSD_M'] - mst_ret_table['RISKFREE_M']
        mst_ret_table['RETURNX'] = mst_ret_table['SGPMKT_M'] - mst_ret_table['RISKFREE_M']
        mst_ret_table['XM'] = mst_ret_table['RETURNX'].groupby(level='companyId').transform('mean')
        mst_ret_table['YM'] = mst_ret_table['RETURNY'].groupby(level='companyId').transform('mean')
        # deviation

        mst_ret_table['XY'] = (mst_ret_table['RETURNX'] - mst_ret_table['XM']) * (
                mst_ret_table['RETURNY'] - mst_ret_table['YM'])
        mst_ret_table['XX'] = (mst_ret_table['RETURNX'] - mst_ret_table['XM'])**2

        # regression table
        regress_table = mst_ret_table.groupby(level=['referenceDate', 'companyId'])[['XY', 'XX']].sum()

        # beta,  a pandas series
        beta = regress_table['XY'] / regress_table['XX']

        del mst_ret_table
        del regress_table

        # extract market cap from data input
        mcap = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'MCAPUSD_M']
        # drop level N in the column
        mcap.columns = mcap.columns.droplevel(level=1)
        # use sqrt(MCAPUSD) as weight
        sqrt_mcap = np.sqrt(mcap.iloc[:, 0])

        # re-index beta series, fill the beta of companies unqualified for beta calculation in the universe with
        # nan value
        beta = beta.reindex(self.data.index)

        # one vector as group
        ones = pd.Series(np.ones(len(beta)), index=beta.index)

        # get calculation universe
        calc_univ = CompanyUniverse.calc_univ

        # scoring beta
        beta_score = self.score_risk_factor(series=beta, calc_universe=calc_univ, group=ones, weights=sqrt_mcap)

        # name the result pandas series with factor name
        beta, beta_score = self.name_result_series((beta, beta_score))

        return beta, beta_score

