from factor_base import Factor
from data_wrapper import Financial
from sga_python_library.sga_math.sga_math import divide
from data import CompanyUniverse
import numpy as np
import pandas as pd


class OIA(Factor):
    """

    standard deviation of 36 monthly return
    """
    name = 'EREVFY1FY2'
    type = 'ALPHA'
    data_cols = [Financial(data_item='TOTASSETS', period=0, period_type='ANN'),
                 Financial(data_item='SGA', period=0, period_type='ANN', n_period=-10),
                 Financial(data_item='RDEXP', period=0, period_type='ANN', n_period=-10)]
    group_cols = ['SUBINDUSTRY', 'INDUSTRY', 'INDUSTRYGROUP', 'SECTOR']

    def __init__(self, data):
        super().__init__()
        self.data = data

    def compute(self):
        rd_eff_rate = 1
        rd_dep_rate = np.power(0.9, range(10)).reshape(-1, 1)  # 10x1 2d array
        sga_eff_rate = 0.3
        sga_dep_rate = np.power(0.9, range(10)).reshape(-1, 1)  # 10x1 2d array

        # drop column level dataItem, leave level period only
        # (get_avail (MATLAB: SGA.avail) func runs extremely slow here.)
        rda = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'RDEXP_ANN'].fillna(0).droplevel(
            level='dataItem', axis=1)
        sgna = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'SGA_ANN'].fillna(0).droplevel(
            level='dataItem', axis=1)

        sga_other = sgna - rda

        # reorder columns to make sure the order is right for matrix multiplication
        sga_other = sga_other[[0, -1, -2, -3, -4, -5, -6, -7, -8, -9]].to_numpy()
        rda = rda[[0, -1, -2, -3, -4, -5, -6, -7, -8, -9]].to_numpy()

        # be careful when converting bwtween pandas DataFrame/Series, and numpy ndarray/1darrat
        oia = rd_eff_rate * np.matmul(rda, rd_dep_rate) + sga_eff_rate * np.matmul(sga_other, sga_dep_rate)
        oia = pd.Series(oia.flatten())
        oia.index = self.data.index
        oia.name = 'OIA'

        assets = self.data.loc[:, self.data.columns.get_level_values('dataItem') == 'TOTASSETS_ANN'].iloc[:, 0]
        # to use func divide, numerator and denominator must be in the form of Series
        oia_to_asset = divide(oia, assets)

        # one vector
        ones = pd.Series(np.ones(len(oia_to_asset)), index=oia_to_asset.index)

        # get calculation universe
        calc_univ = CompanyUniverse.calc_univ

        # grouping dataframe
        groups = pd.concat([CompanyUniverse.sub_industry, CompanyUniverse.industry, CompanyUniverse.industry_group,
                            CompanyUniverse.sector], axis=1)

        # scoring OIA
        oia_score = self.score_alpha_factor(data=oia_to_asset, calc_universe=calc_univ, groups=groups,
                                            strategy_universe=ones)

        # name the result pandas series with factor name
        oia_to_asset, oia_score = self.name_result_series((oia_to_asset, oia_score))
        return oia_to_asset, oia_score
