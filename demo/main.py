from model import ModelManager
from demo.GLBET import GLBET
from demo.EREV import EREV
from demo.OIA import OIA
from demo.EPMOM import EPMOM
import warnings
import pandas as pd
warnings.filterwarnings("ignore")


# --------------------------------------CASE 1: Risk Factor: Global Beta (GLBET)---------------------------------
# CASE 1: Risk Factor: Global Beta (GLBET)
# to check how GLBET is computed, press Ctrl then click on GLBET

m1 = ModelManager(universe=['ALL'], factor_list=[GLBET], start_date='2020-01-31', end_date='2020-03-31',
                  keep_inputs_flag=True)
# select a single date to start testing
# step 1: import data
m1.import_data(pd.to_datetime('2020-01-31'))
# step 2: filter and clean data inputs for factor
glbet_data = m1.get_data_single_fac_single_date(fac=GLBET, dt=pd.to_datetime('2020-01-31'))
# step 3: use the factor data inputs to test compute func inside GLBET
glbet_test = m1.calc_single_fac_single_date(fac=GLBET, fac_data_inputs=glbet_data)

# run the model from start date to end date, check result by calling m1.values, m1.scores, m1.factor_data_inputs
m1.run_model()
# ----------------------------------------------------------------------------------------------------------------------


# -------------------------------------CASE 2: Alpha factor: Omitted Intangible Assets (OIA)----------------------------
# to check how OIA is computed, press Ctrl then click on OIA

m2 = ModelManager(universe=['ALL'], factor_list=[OIA], start_date='2001-01-31', end_date='2001-01-31',
                  keep_inputs_flag=True)
# select a single date to start testing
# step 1: import data
m2.import_data(pd.to_datetime('2001-01-31'))
# step 2: filter and clean data inputs for factor
oia_data = m2.get_data_single_fac_single_date(fac=OIA, dt=pd.to_datetime('2001-01-31'))
# step 3: use the factor data inputs to test compute func inside OIA
# todo: an issue inside sga_python_library 'get_univ_group_weighted_score_subindustry' need to be fixed (2021/01/19)
# oia_test = m2.calc_single_fac_single_date(fac=OIA, fac_data_inputs=oia_data)
# ----------------------------------------------------------------------------------------------------------------------

# -------------------------------------CASE 3: Alpha factor: Earnings Revision (EREV)----------------------------
# to check how EREV is computed, press Ctrl then click on EREV

m3 = ModelManager(universe=['ALL'], factor_list=[EREV], start_date='2020-01-31', end_date='2020-01-31',
                  keep_inputs_flag=True)
# select a single date to start testing
# step 1: import data
m3.import_data(pd.to_datetime('2020-01-31'))
# step 2: filter and clean data inputs for factor
erev_data = m3.get_data_single_fac_single_date(fac=EREV, dt=pd.to_datetime('2020-01-31'))
# step 3: use the factor data inputs to test compute func inside OIA
# erev_test = m2.calc_single_fac_single_date(fac=EREV, fac_data_inputs=erev_data)
# ----------------------------------------------------------------------------------------------------------------------


#-------------------------------------CASE 4: Alpha factor: EP Momentum (EPMOM)----------------------------
m4 = ModelManager(universe=['ALL'], factor_list=[EPMOM], start_date='2001-01-31', end_date='2001-01-31',
                  keep_inputs_flag=True)
# select a single date to start testing
# step 1: import data
m4.import_data(pd.to_datetime('2001-01-31'))
# step 2: filter and clean data inputs for factor
epmom_data = m4.get_data_single_fac_single_date(fac=EPMOM, dt=pd.to_datetime('2001-01-31'))
# step 3: use the factor data inputs to test compute func inside OIA
# todo: an issue inside sga_python_library 'get_univ_group_weighted_score_subindustry' need to be fixed (2021/01/19)
# epmom_test = m2.calc_single_fac_single_date(fac=EREV, fac_data_inputs=erev_data)
# ----------------------------------------------------------------------------------------------------------------------