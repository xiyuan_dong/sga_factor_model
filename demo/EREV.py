from factor_base import Factor
from data_wrapper import Estimates
from sga_python_library.sga_math.sga_math import divide
from data import CompanyUniverse
import numpy as np
import pandas as pd


class EREV(Factor):
    """
    Earnings Revision
    #todo: to be updated after SGA_ESTIMATE_NEST_AVAIL has been updated to SGA_ESTIMATE_NEST_AVAIL_20201228
    """

    name = 'EREV'
    type = 'ALPHA'
    data_cols = [Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='UP'),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='UP'),
                 Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='DOWN'),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='DOWN'),
                 Estimates(data_item='EPS', fwd_period=1, period_type='ANN', statistic='NEST'),
                 Estimates(data_item='EPS', fwd_period=2, period_type='ANN', statistic='NEST')
                 ]

    def __init__(self, data_set):
        self.data_set = data_set

    def compute(self):
        nest = self.data_set.iloc[:, 4] + self.data_set.iloc[:, 5]
        up = self.data_set.iloc[:, 0] + self.data_set.iloc[:, 1]
        down = self.data_set.iloc[:, 2] + self.data_set.iloc[:, 3]
        erev = divide(up-down, nest)
        set1 = nest[nest < 3].index
        set2 = erev[erev != 0].index
        set0 = set1.intersection(set2)
        if not set0.empty:
            erev.loc[set0] = np.nan

        ones = pd.Series(np.ones(len(erev)), index=erev.index)
        # get calculation universe
        calc_univ = CompanyUniverse.calc_univ

        # grouping dataframe
        groups = pd.concat([CompanyUniverse.sub_industry, CompanyUniverse.industry, CompanyUniverse.industry_group,
                            CompanyUniverse.sector], axis=1)

        # scoring
        erev_score = self.score_alpha_factor(data=erev, calc_universe=calc_univ, groups=groups, strategy_universe=ones)

        # name the result pandas series with factor name
        erev, erev_score = self.name_result_series((erev, erev_score))
        return erev, erev_score
