from sga_python_library.sga_score.univ_group_weighted_score import get_univ_group_weighted_score
from sga_python_library.sga_score.univ_group_weighted_subindustry_score import get_univ_group_weighted_score_subindustry


class Factor:
    default_cols = ['referenceDate', 'companyId']
    group_cols = 'default'
    data_cols = []
    name = ''
    type = ''

    def __init__(self):
        pass

    @classmethod
    def name_result_series(cls, tuple_of_series):
        for s in tuple_of_series:
            s.name = cls.name
        return tuple_of_series

    @staticmethod
    def score_risk_factor(**kwargs):
        return get_univ_group_weighted_score(**kwargs)

    @staticmethod
    def score_alpha_factor(**kwargs):
        return get_univ_group_weighted_score_subindustry(n_companies_full_weight=20,  n_min_companies=10, **kwargs)

