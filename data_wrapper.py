from data_config import estimates_data_item_dict, period_type_dict
import pandas as pd
"""
data_wrapper
---------
"""


class DataWrapper:
    """
    A wrapper for users to pass input to construct factors
    """
    @classmethod
    def expand_periods(cls, period, n_period):
        if n_period is None:
            return [period]
        else:
            return [period + i + 1 for i in range(n_period, 0)]


class Financial(DataWrapper):
    data_class = 'CAPIQ_Financial'

    def __init__(self, data_item: str, period: int, period_type: str, n_period: int = None):
        """

        :param data_item: data item name could be found in data_config.financial_data_item_dict
        :param period: 0 denotes the most recent financial period as of each month end.
                        + / - : looking forward (forward-looking IS NOT available for now) or backward
                        e.g. period = -1 denotes 1 period before the most recent period
        :param period_type: available option: 'ANN' / 'QTR' / 'LTM' / 'SA'
                            ANN: annual
                            QTR: quarterly
                            LTM: last twelve month.
                            SA : semi-annually

        :param n_period: used when multiple periods data needed.
                        + / - : looking forward (forward-looking IS NOT available for now) or backward
                        e.g. when you need past 10 years's R&D Expense (OIA):
                             set period = 0 & n_period = -10
                             if you only need one period:
                             set period = x and do not pass value to n_period
        """
        self.data_item = data_item
        self.period = period
        self.period_type = period_type
        self.n_period = n_period
        self.period_list = DataWrapper.expand_periods(self.period, self.n_period)
        self.name = data_item+'_' + period_type


class Pricing(DataWrapper):
    data_class = 'CAPIQ_Pricing'

    def __init__(self, data_item: str, period: int, period_type: str, n_period: int = None):
        """

        :param data_item:  data item name could be found in data_config.pricing_data_item_dict
        :param period: 0 denotes the most recent  period as of each month end.
                        + / - : looking forward (forward-looking IS NOT available for now) or backward
                        e.g. period = -1 denotes 1 period before the most recent period
        :param period_type: available option: 'M'/ 'D'
        :param n_period: used when multiple periods data needed.
                        + / - : looking forward (forward-looking IS NOT available for now) or backward
                        e.g. when you need past 60 month's total return (GLEBT):
                             set period = 0 & n_period = -60
                             if you only need one period:
                             set period = x and do not pass value to n_period
        """
        self.data_item = data_item
        self.period = period
        self.period_type = period_type
        self.n_period = n_period
        self.period_list = DataWrapper.expand_periods(self.period, self.n_period)
        self.name = data_item+'_' + period_type


class Estimates(DataWrapper):
    #
    # Default: 100 days prior to observation date
    # todo: add user-defined aggregation function as an argument to initialize an Estimates data class

    data_class = 'CAPIQ_Estimates'

    CONSENSUS_TYPE = ['MEAN', 'MEDIAN', 'HIGH', 'LOW', 'STD', 'NEST', 'SIGMA', 'STDDEV']
    REVISION_TYPE = ['UP', 'DOWN', 'UNCHGED']

    # FactSet methodology default values:
    CONSENSUS_WINDOW = 100
    REVISION_WINDOW = 75
    PR_WINDOW = 105

    # Lists of analysts or brokers to be excluded
    ANALYSTS_EXCL = []
    BROKERS_EXCL = []

    def __init__(self, data_item: str, fwd_period: int, period_type: str, statistic: str, lag: int = 0,
                 window=None, analysts_excl=None, brokers_excl=None):
        """

        :param data_item: data item name could be found in data_config.estimates_data_item_dict
        :param fwd_period: forwarding looking period
                            e.g. period = 1 denotes forward one period
        :param period_type: available options: 'ANN' for now
                            will add more data of other frequency (QTR/LTM/Calendar Year/Non-Periodic/Semi-Annual)
                            to database very soon

        :param statistic: available options: 'MEAN', 'MEDIAN', 'HIGH', 'LOW', 'STD', 'NEST', 'SIGMA', 'STDDEV'
        :param lag: a positive integer to specify how many day lagged
                    e.g.: lag = 90 means 90 days lagged
        :param window: choose the estimate window [short, long]  for effective analyst estimates
                       don't pass value to this argument if you don't want change the default setting: [-100, 0]
                       e.g.: [-100, -30] means only use analyst estimates made from 100 days to 30 days prior to
                            observation date
        :param analysts_excl: a list of analyst id (integer) to be excluded. Default: none
        :param brokers_excl: a list of broker id (integer) to be excluded. Default: none
        """
        self.data_item = data_item
        self.fwd_period = fwd_period
        self.period_type = period_type
        self.window = window if window is not None else [-1*self.CONSENSUS_WINDOW, 0]
        self.lag = lag
        self.analysts_excl = analysts_excl if analysts_excl is not None else Estimates.ANALYSTS_EXCL
        self.brokers_excl = brokers_excl if brokers_excl is not None else Estimates.BROKERS_EXCL
        self.statistic = statistic
        self.name = data_item+'_' + period_type + '_' + statistic + '_'+str(abs(self.window[1])) + str(
            abs(self.window[0])) + '_L' + str(lag)+'D'
        self.period_list = [self.fwd_period]

    # filter out effective analyst estimates
    def filter_data(self, raw_data):
        # Step 1: data item -> forward period -> period type -> excluded analyst & broker
        data_item = estimates_data_item_dict[self.data_item]
        fwd_period = self.fwd_period
        period_type = period_type_dict[self.period_type]
        analysts_excl = self.analysts_excl
        brokers_excl = self.brokers_excl
        df = raw_data.loc[(raw_data.dataItemId == data_item) & (raw_data.N == fwd_period)
                          & (raw_data.periodTypeId == period_type)
                          & (~raw_data.estimateAnalystId.isin(analysts_excl))
                          & (~raw_data.estimateBrokerId.isin(brokers_excl)), :]

        # Step 2: if lagged, filter out effective analyst estimates with estimates date
        if self.lag != 0:
            df['lagFlag'] = (df['estimateDate'] <= df['referenceDate'] + pd.DateOffset(days=self.lag)) & \
                            (df['toDate'] >= df['referenceDate'] + pd.DateOffset(days=self.lag))
            df = df.loc[(df.lagFlag == 1), :].reset_index(drop=True)
            # Re-rank the remaining estimates made by the same broker
            df["rnk"] = df.groupby(['companyId', 'estimateBrokerId'])['estimateDate'].rank("dense", ascending=False)
            df.drop(columns=['lagFlag'], inplace=True)

        # Step 3: if the statistics is a consensus, only keep the most updated effective estimates within the window
        if self.statistic not in self.REVISION_TYPE:
            df = df.loc[(df.rnk == 1), :].reset_index(drop=True)
            # toDate >= referenceDate - lag to make sure the estimates are still in effect
            # effectiveDate>=referenceDate - long_window - lag to make sure the estimates are reported within the window
            df['longFlag'] = (df['toDate'] >= df['referenceDate'] + pd.DateOffset(days=self.lag))\
                & (df['effectiveDate'] >= df['referenceDate'] + pd.DateOffset(days=(self.window[0] + self.lag)))
            df['shortFlag'] = (df['toDate'] >= df['referenceDate'] + pd.DateOffset(days=self.lag)) \
                & (df['effectiveDate'] >= df['referenceDate'] + pd.DateOffset(days=(self.window[1] + self.lag)))
            if self.window[1] == 0:
                df = df.loc[df.longFlag == 1, :].reset_index(drop=True)
            else:
                df = df.loc[(df.longFlag == 1) & (df.shortFlag == 0), :].reset_index(drop=True)
            df.drop(columns=['longFlag', 'shortFlag', 'rnk'], inplace=True)
        else:
            df = df.loc[df.rnk.isin([1, 2]), :].reset_index(drop=True)
        return df

    def cal_statistics(self, raw_data):
        df = self.filter_data(raw_data)
        s = None
        if self.statistic in self.REVISION_TYPE:
            s = self.cal_revision(df)
        if self.statistic in self.CONSENSUS_TYPE or self.statistic.replace('.', '', 1).isdigit():
            s = self.cal_consensus(df)
        result = s.to_frame(name='dataItemValue')
        result['dataItem'] = self.name
        result['N'] = self.fwd_period
        return result

    def cal_consensus(self, df):

        if self.statistic == 'MEDIAN':
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].median()

        if self.statistic == 'MEAN':
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].mean()

        if self.statistic == 'HIGH':
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].max()

        if self.statistic == 'LOW':
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].low()

        if self.statistic == 'STDDEV':
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].std()

        if self.statistic == 'NEST':
            return df.groupby(['referenceDate', 'companyId'])['estimateBrokerId'].count()

        if self.statistic.replace('.', '', 1).isdigit():
            return df.groupby(['referenceDate', 'companyId'])['dataItemValue'].quantile(float(self.statistic))

    def cal_revision(self, df):
        latest = df.loc[df.rnk == 1, ['referenceDate', 'companyId', 'estimateBrokerId', 'dataItemValue',
                                      'effectiveDate', 'toDate', 'estimateDate']]
        previous = df.loc[df.rnk == 2, ['referenceDate', 'companyId', 'estimateBrokerId', 'dataItemValue',
                                        'effectiveDate', 'toDate', 'estimateDate']]
        comp = pd.merge(latest, previous, how='left', on=['referenceDate', 'companyId', 'estimateBrokerId'],
                        suffixes=['_1', '_2'])
        if self.statistic in ['UP', 'DOWN']:

            # FactSet Methodology: Determining an Upward or Downward Revision
            # 1.Check if the latest estimate is valid:
            #       The latest estimate occurred within the consensus window as of the perspective date.
            # 2.Check if there was a estimate modification of the estimate within the revision window:
            #       The latest modification (LM) occurred within the revision window as of the perspective date.
            # 3.Check if the previous estimate is valid:
            #       The previous research date (PR) occurred within the consensus window plus 105 days
            #       from the perspective date.

            step1 = (comp['effectiveDate_1'] >= comp['referenceDate'] + pd.DateOffset(
                days=(-1*self.CONSENSUS_WINDOW+self.lag))) & \
                    (comp['toDate_1'] >= comp['referenceDate'] + pd.DateOffset(days=self.lag))

            step2 = (comp['estimateDate_1'] >= comp['referenceDate'] + pd.DateOffset(
                days=(-1 * self.REVISION_WINDOW + self.lag)))

            step3 = (comp['effectiveDate_2'] >= comp['referenceDate'] + pd.DateOffset(
                days=(-1 * self.CONSENSUS_WINDOW - self.PR_WINDOW + self.lag)))
            if self.statistic == 'UP':
                direction = comp['dataItemValue_1'] > comp['dataItemValue_2']
            else:
                direction = comp['dataItemValue_1'] < comp['dataItemValue_2']
            comp['isValid'] = step1 & step2 & step3 & direction

            return comp.loc[(comp.isValid == 1), ['referenceDate', 'companyId', 'estimateBrokerId']].groupby(
                ['referenceDate', 'companyId'])['estimateBrokerId'].count()

