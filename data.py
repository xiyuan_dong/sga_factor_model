from sqlalchemy import create_engine
from data_config import financial_data_item_dict, pricing_data_item_dict, estimates_data_item_dict
from functools import partial
import pandas as pd
import xml.etree.cElementTree as ET


class Data:
    identifier = ''
    date = ''
    conn_str = 'mssql+pyodbc:///?odbc_connect=DRIVER%3D%7BODBC Driver 17 for SQL Server%7D%3BSERVER%3D' \
                 'SGA-Research-01%3BPORT%3D1433%3BDATABASE%3DResearchDB%3BUID%3Dcmp%3BPWD%3Dsga%3B'
    date = ''
    sga_research_engine = create_engine(conn_str)

    @classmethod
    def set_date(cls, date):
        cls.date = date

    @classmethod
    def set_identifier(cls, identifier):
        # todo: enable choosing other identifier other than companyId, e.g. SEDOL/CUSIP/ISIN
        cls.identifier = identifier

    @classmethod
    def set_conn_str(cls, conn_str):
        cls.conn_str = conn_str

    @classmethod
    def convert_date_type(cls, df):
        """
        :param df: raw_data
        :return: turn date data from object type to pandas datetime
        """
        date_cols = df.columns.str.endswith('Date')
        df.loc[:, date_cols] = df.loc[:, date_cols].apply(pd.to_datetime, errors='coerce')
        return df

    @classmethod
    def convert_value_type(cls, df):
        """
        :param df: raw_data
        :return: turn value data from object type to numeric
        """
        date_cols = df.columns.str.endswith('Value')
        df.loc[:, date_cols] = df.loc[:, date_cols].apply(pd.to_numeric, errors='coerce')
        return df


class CompanyUniverse(Data):
    name = 'CompanyUniverse'
    _loaded_flag = False
    univ = None
    calc_univ = None
    country = None
    region = None
    sector = None
    industry_group = None
    industry = None
    sub_industry = None
    size_group = None

    def __init__(self):
        pass

    @classmethod
    def set_data(cls, univ: [str] = ['ALL']):
        # todo: enable choosing universe
        try:
            query = """
             select referenceDate, companyId, CALCUNIV, COUNTRY, REGION, SECTOR, INDUSTRYGROUP, INDUSTRY, SUBINDUSTRY,
                    SIZEGROUP
             from [ResearchDB].[dbo].[CIQ_Company_Universe] cu
             where referenceDate='{}'
                    and UNIV=1
            """.format(CompanyUniverse.date)
            data = pd.read_sql(query, cls.sga_research_engine)
            cls._loaded_flag = True
            data = data.drop_duplicates().reset_index(drop=True)
            data = Data.convert_date_type(df=data)
            cls.univ = data.loc[:, ['referenceDate', 'companyId']].drop_duplicates()
            cls.calc_univ = cls._get_column(raw_data=data, data_item='CALCUNIV')
            cls.country = cls._get_column(raw_data=data, data_item='COUNTRY')
            cls.region = cls._get_column(raw_data=data, data_item='REGION')
            cls.sector = cls._get_column(raw_data=data, data_item='SECTOR')
            cls.industry_group = cls._get_column(raw_data=data, data_item='INDUSTRYGROUP')
            cls.industry = cls._get_column(raw_data=data, data_item='INDUSTRY')
            cls.sub_industry = cls._get_column(raw_data=data, data_item='SUBINDUSTRY')
            cls.size_group = cls._get_column(raw_data=data, data_item='SIZEGROUP')
            del data
        except:
            print('Company Universe loading failed')

    @classmethod
    def _get_column(cls, raw_data: pd.DataFrame, data_item: str) -> pd.Series:
        # todo: check for 1-to-many in SECTOR, INDUSTRYRGOUP, INDUSTRY, SUBINDUSTRY, SIZEGROUP
        return raw_data.loc[:, ['referenceDate', 'companyId'] + [data_item]].dropna(subset=['companyId']). \
            drop_duplicates(subset=['referenceDate', 'companyId'], keep='first'). \
            set_index(['referenceDate', 'companyId']).iloc[:, 0].astype('float')


class CIQExchangeRate(Data):
    # todo: to be updated. FX data should be in daily frequency
    """
    load Exchange rate at each month end between research start date and end date for any currency conversion
    """
    data = None
    name = 'CIQExchangeRate'
    loaded = False
    freq = ['M']

    def __init__(self):
        pass

    @classmethod
    def set_data(cls, freq='M'):
        try:
            conn = Data.sga_research_engine.raw_connection()
            cursor = conn.cursor()
            query = 'exec [CIQ_ExRate_Data_Retrieve] ?, ?, ?'
            cursor.execute(query, freq, CIQExchangeRate.first_date, CIQExchangeRate.last_date)
            if freq == 'M':
                cls.monthly_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.monthly_data.columns = [column[0] for column in cursor.description]
                if not cls.monthly_data.empty:
                    cls.loaded = True
            if freq == 'D':
                cls.freq.append(freq)
                cls.daily_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.daily_data.columns = [column[0] for column in cursor.description]
                if not cls.daily_data.empty:
                    cls.loaded = True
            cursor.close()
        except:
            print('CIQ Exchange Rate data loading failed')


class CIQFinancialData(Data):
    freq = ['ANN', 'QTR', 'SA', 'LTM']
    ann_data = None
    ltm_data = None
    qtr_data = None
    sa_data = None
    freq = ['ANN', 'LTM', 'QTR', 'SA']
    name = 'CAPIQ_Financial'
    _loaded_flag = False

    def __init__(self):
        pass

    @classmethod
    def set_data(cls, paras):
        """
        :param paras: data item and period parsed by Model Manager
        :return:
        """

        gen_data_func = {
            'ANN': partial(cls._gen_data,  period_type='ANN'),
            'LTM': partial(cls._gen_data,  period_type='LTM'),
            'QTR': partial(cls._gen_data,  period_type='QTR'),
            'SA': partial(cls._gen_data,  period_type='SA')
        }
        for period_type in paras.keys():
            gen_data_func[period_type](para=paras[period_type])
        cls._loaded_flag = True

    @classmethod
    def _gen_data(cls, para, period_type):
        sql_proc_dict = {
            'ANN': 'exec [CIQ_Fin_Data_Retrieve_ANN] ?',
            'LTM': 'exec [CIQ_Fin_Data_Retrieve_LTM] ?',
            'QTR': 'exec [CIQ_Fin_Data_Retrieve_QTR] ?',
            'SA': 'exec [CIQ_Fin_Data_Retrieve_SA] ?'
        }
        try:
            conn = Data.sga_research_engine.raw_connection()
            cursor = conn.cursor()
            xml_para = CIQFinancialData._generate_sql_xml_para(para)
            query = sql_proc_dict[period_type]
            cursor.execute(query, xml_para)
            if period_type == 'ANN':
                cls.ann_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.ann_data.columns = [column[0] for column in cursor.description]
                cursor.close()
                cls.ann_data = Data.convert_date_type(df=cls.ann_data)
                cls.ann_data = Data.convert_value_type(df=cls.ann_data)
            if period_type == 'LTM':
                cls.ltm_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.ltm_data.columns = [column[0] for column in cursor.description]
                cursor.close()
                cls.ltm_data = Data.convert_date_type(df=cls.ltm_data)
                cls.ltm_data = Data.convert_value_type(df=cls.ltm_data)
            if period_type == 'QTR':
                cls.qtr_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.qtr_data.columns = [column[0] for column in cursor.description]
                cursor.close()
                cls.qtr_data = Data.convert_date_type(df=cls.qtr_data)
                cls.qtr_data = Data.convert_value_type(df=cls.qtr_data)
            if period_type == 'SA':
                cls.sa_data = pd.DataFrame.from_records(cursor.fetchall())
                cls.sa_data.columns = [column[0] for column in cursor.description]
                cursor.close()
                cls.sa_data = Data.convert_date_type(df=cls.sa_data)
                cls.sa_data = Data.convert_value_type(df=cls.sa_data)
        except:
            print('CIQ Financial '+period_type+' data loading failed')

    @staticmethod
    def _generate_sql_xml_para(para):
        data_id_list = [financial_data_item_dict[i] for i in para.keys()]
        period_list = []
        for value in para.values():
            period_list.extend(value)
        period_list = list(set(period_list))
        inputs = (data_id_list, period_list, CIQFinancialData.date)
        return ET.tostring(CIQFinancialData._create_para_xml(inputs).getroot(),
                           encoding='utf-8', method='xml').decode("utf-8")

    @staticmethod
    def _create_para_xml(inputs):
        root = ET.Element('root')
        data_item = ET.SubElement(root, "dataitem")
        latest = ET.SubElement(root, "latest")
        ET.SubElement(root, "date").text = str(inputs[2])
        for i in inputs[0]:
            ET.SubElement(data_item, "dataitemid").text = str(i)
        for j in inputs[1]:
            ET.SubElement(latest, "period").text = str(j)
        tree = ET.ElementTree(root)
        return tree


class CIQPricingData(Data):
    freq = ['M', 'D']
    monthly_data = None
    daily_data = None
    name = 'CAPIQ_Pricing'
    _loaded_flag = False

    def __init__(self):
        pass

    @classmethod
    def set_data(cls, paras):
        gen_data_func = {
            'M': cls._gen_data_monthly,
            'D': cls._gen_data_daily
        }
        for period_type in paras.keys():
            gen_data_func[period_type](paras[period_type])
        _loaded_flag = True

    @classmethod
    def _gen_data_monthly(cls, para_m):
        try:
            conn = Data.sga_research_engine.raw_connection()
            cursor = conn.cursor()
            para = CIQPricingData._generate_sql_xml_para(para_m)
            query = 'exec [CIQ_Pricing_Data_Monthly_Retrieve] ?'
            cursor.execute(query, para)
            cls.monthly_data = pd.DataFrame.from_records(cursor.fetchall())
            cls.monthly_data.columns = [column[0] for column in cursor.description]
            cursor.close()
            cls.monthly_data = Data.convert_date_type(df=cls.monthly_data)
        except:
            print('CIQ Pricing Monthly data loading failed')

    @classmethod
    def _gen_data_daily(cls, para_d):
        try:
            conn = Data.sga_research_engine.raw_connection()
            cursor = conn.cursor()
            para = CIQPricingData._generate_sql_xml_para(para_d)
            query = 'exec [CIQ_Pricing_Data_Daily_Retrieve] ?'
            cursor.execute(query, para)
            cls.daily_data = pd.DataFrame.from_records(cursor.fetchall())
            cls.daily_data.columns = [column[0] for column in cursor.description]
            cursor.close()
            cls.daily_data = Data.convert_date_type(df=cls.daily_data)
        except:
            print('CIQ Pricing Daily data loading failed')

    @staticmethod
    def _create_para_xml(inputs):
        root = ET.Element('root')
        latest = ET.SubElement(root, "latest")
        ET.SubElement(root, "date").text = str(inputs[0])
        for j in inputs[1]:
            ET.SubElement(latest, "period").text = str(j)
        tree = ET.ElementTree(root)
        return tree

    @staticmethod
    def _generate_sql_xml_para(paras):
        period_list = []
        for value in paras.values():
            period_list.extend(value)
        period_list = list(set(period_list))
        inputs = (Data.date, period_list)
        return ET.tostring(CIQPricingData._create_para_xml(inputs).getroot(),
                           encoding='utf-8', method='xml').decode("utf-8")


class CIQEstimatesData(Data):
    # todo: add data of other frequency
    # todo: modify data retrieve method for data items other than EPS (need to choose primary accounting standards)
    data = None
    freq = ['ANN', 'QTR', 'LTM', 'SA', 'CY', 'NP']
    # CY: Calendar Year
    # NP: Non-periodic
    name = 'CAPIQ_Estimates'
    loaded = False

    @classmethod
    def set_data(cls, para):
        if para['lag'] != [0]:
            series = ''
        elif any(t in para['statistic'] for t in ['UP', 'DOWN', 'COUNT']):
            series = '1, 2'
        else:
            series = '1'
        fwd_periods = ','.join([str(i) for i in para['fwd_period']])

        try:
            conn = Data.sga_research_engine.raw_connection()
            cursor = conn.cursor()
            query = 'exec [CIQ_Estimates_Data_Retrieve] ?, ?, ?, ?, ?'
            cursor.execute(query, 'EPS', CIQPricingData.date, 1, fwd_periods, series)
            cls.data = pd.DataFrame.from_records(cursor.fetchall())
            cls.data.columns = [column[0] for column in cursor.description]
            cursor.close()
            cls.data = Data.convert_date_type(df=cls.data)
            cls.data = Data.convert_value_type(df=cls.data)
        except:
            print('CIQ Analyst Estimates data loading failed')


class CurrencyMap(Data):
    data = None
    name = 'CurrencyMap'
    loaded = False

    @classmethod
    def set_data(cls):
        # todo: To build up a currency map for time-dependent primary financial reporting currency, and other ...
        pass




