import pandas as pd
import logging
# todo: set up logging
import time
from filter import FilterManager
from data import Data, CIQFinancialData, CIQPricingData, CIQEstimatesData, CompanyUniverse
from calc import CalcManager
from datetime import timedelta
from dateutil.rrule import rrule, MONTHLY
from typing import List, Type
from factor_base import Factor
from data_wrapper import Financial, Pricing, Estimates


class ModelManager:
    IDENTIFIER = 'companyId'
    DATE = 'referenceDate'

    def __init__(self, universe: List[str], factor_list: List[Type[Factor]], start_date: str, end_date: str,
                 keep_inputs_flag: bool = False):
        self.universe = universe
        self.factor_list = factor_list
        self.start_date = start_date
        self.end_date = end_date
        self.date_list = self._generate_ref_dates()
        self.financial, self.pricing, self.estimates, self.estimates_cols, self.data_class_list = \
            self._parse_factor_input()
        self.values = pd.DataFrame()
        self.scores = pd.DataFrame()
        self.keep_inputs_flag = keep_inputs_flag
        self.factor_data_inputs = None
        self.__run_status = False

    def _generate_ref_dates(self):
        """
        Given start date and end date, generate all month end date as reference date in between in the format of pandas
        datatime data type
        :return: date_list, list of datetime
        """
        s = pd.to_datetime(self.start_date)+timedelta(days=1)
        e = pd.to_datetime(self.end_date)+timedelta(days=1)
        date_list = [pd.to_datetime(dt + timedelta(days=-1)) for dt in rrule(MONTHLY, dtstart=s, until=e)]
        return date_list

    def _parse_factor_input(self):
        """
        Given the factor list that the model manager takes, parse the input of factors for importing data from database,
        and pre-processing raw data

        :return:

        financial/pricing: dictionaries for importing and pre-processing data
        e.g.
        {
            'LTM': {
                'net_inc' : [0, -1],
                'tot_asset: [0, -1, -2]
            },
            'QTR':{
                'net_inc' : [0, -1],
                'tot_asset: [0, -1, -2]
            }
        }

        estimates: dictionary for importing data
        estimates_cols: list for pre-processing data
        """
        factor_list = self.factor_list
        financial = {}
        pricing = {}
        estimates = {'data_item': [],
                     'period_type': [],
                     'fwd_period': [],
                     'lag': [],
                     'statistic': []}
        estimates_cols = []
        data_class_list = []

        for fac in factor_list:
            cols = fac.data_cols
            for col in cols:
                if isinstance(col, Financial):
                    if 'CAPIQ_Financial' not in data_class_list:
                        data_class_list.append('CAPIQ_Financial')
                    if col.period_type not in financial.keys():
                        financial[col.period_type] = {}
                    if col.data_item not in financial[col.period_type].keys():
                        financial[col.period_type][col.data_item] = col.period_list
                    if not all(item in col.period_list for item in financial[col.period_type][col.data_item]):
                        financial[col.period_type][col.data_item].extend(col.period_list)
                        financial[col.period_type][col.data_item] = list(set(financial[col.period_type][col.data_item]))

                if isinstance(col, Pricing):
                    if 'CAPIQ_Pricing' not in data_class_list:
                        data_class_list.append('CAPIQ_Pricing')
                    if col.period_type not in pricing.keys():
                        pricing[col.period_type] = {}
                    if col.data_item not in pricing[col.period_type].keys():
                        pricing[col.period_type][col.data_item] = col.period_list
                    if not all(item in pricing[col.period_type][col.data_item] for item in col.period_list):
                        pricing[col.period_type][col.data_item].extend(col.period_list)
                        pricing[col.period_type][col.data_item] = list(set(pricing[col.period_type][col.data_item]))

                if isinstance(col, Estimates):
                    estimates_cols.append(col)
                    if 'CAPIQ_Estimates' not in data_class_list:
                        data_class_list.append('CAPIQ_Estimates')
                    if col.period_type not in estimates['period_type']:
                        estimates['period_type'].append(col.period_type)
                    if col.data_item not in estimates['data_item']:
                        estimates['data_item'].append(col.data_item)
                    if col.fwd_period not in estimates['fwd_period']:
                        estimates['fwd_period'].append(col.fwd_period)
                    if col.lag not in estimates['lag']:
                        estimates['lag'].append(col.lag)
                    if col.statistic not in estimates['statistic']:
                        estimates['statistic'].append(col.statistic)

        return financial, pricing, estimates, estimates_cols, data_class_list

    def import_data(self, date):
        str_date = date.strftime('%Y-%m-%d')
        Data.set_date(date=str_date)

        # Import Financial/Pricing/Estimates data
        data_set_func = {'CAPIQ_Financial': CIQFinancialData.set_data,
                         'CAPIQ_Pricing': CIQPricingData.set_data,
                         'CAPIQ_Estimates': CIQEstimatesData.set_data}
        para = {'CAPIQ_Financial':  self.financial,
                'CAPIQ_Pricing': self.pricing,
                'CAPIQ_Estimates': self.estimates}
        for data_class in self.data_class_list:
            try:
                data_set_func[data_class](para[data_class])
            except Exception as e:
                print(data_class + 'data setting failed!')
                logging.exception(e)

        # Import universe
        CompanyUniverse.set_data(univ=self.universe)

        # Import currency map & exchange rate table
        # todo: Update currency converter

    def _run_filter(self, date):
        # Create a filter instance to filter and preprocess data for calculation
        f = FilterManager(universe=self.universe, factor_list=self.factor_list, date=date,
                          financial=self.financial, pricing=self.pricing, estimates=self.estimates_cols,
                          data_class_list=self.data_class_list)

        return f.filter_all()

    def _calculate_all(self):
        # Process data & calculate factors month by month
        df_values = pd.DataFrame()
        df_scores = pd.DataFrame()
        factor_data_inputs = {}
        for fac in self.factor_list:
            factor_data_inputs[fac.name] = pd.DataFrame()

        for dt in self.date_list:
            temp = self._calculate_single_date(dt=dt)
            df_values = pd.concat([temp[0], df_values], axis=0, ignore_index=False)
            df_scores = pd.concat([temp[1], df_values], axis=0, ignore_index=False)
            if self.keep_inputs_flag:
                for fac in self.factor_list:
                    factor_data_inputs[fac.name] = pd.concat([temp[2], factor_data_inputs[fac.name]], axis=0,
                                                             ignore_index=False)
        return df_values, df_scores, factor_data_inputs

    def _calculate_single_date(self, dt):
        # For testing purpose

        factor_data_inputs = {}
        for fac in self.factor_list:
            factor_data_inputs[fac.name] = pd.DataFrame()

        start_time = time.time()
        # Step 1: import data from database
        print('* * Start job for Year {yr} Month {mon}'.format(yr=dt.year, mon=dt.month))

        self.import_data(date=dt)
        print('* * * Complete data importing for Year {yr} Month {mon}. Time taken: {seconds} s.'.format(
            yr=dt.year, mon=dt.month, seconds=time.time()-start_time))
        new_start_time = time.time()

        # Step 2: filter and preprocess raw data for calculation
        filtered_data_date = self._run_filter(date=dt)
        print('* * * Complete data filtering and cleaning for Year {yr} Month {mon}. Time taken: {seconds} s.'.format(
            yr=dt.year, mon=dt.month, seconds=time.time()-new_start_time))
        new_start_time = time.time()

        # Step 3: Create a CalManager instance and run the job
        calc_obj = CalcManager(factor_list=self.factor_list, filtered_data_date=filtered_data_date, date=dt,
                               keep_inputs_flag=self.keep_inputs_flag)
        df_values, df_scores, factor_data_inputs = calc_obj.calc_all()

        print('* * * Complete factor calculation for Year {yr} Month {mon}. Time taken: {seconds} s.'.format(
            yr=dt.year, mon=dt.month, seconds=time.time() - new_start_time))

        print('* *Finish job for Year {yr} Month {mon}. Total time taken: {seconds}'.format(
            yr=dt.year, mon=dt.month, seconds=time.time()-start_time))
        return df_values, df_scores, factor_data_inputs

    def get_data_single_fac_single_date(self, fac: Type[Factor], dt) -> pd.DataFrame:
        # for testing purpose

        # create a Filter Manager instance with only one factor for the given date
        f = FilterManager(universe=self.universe, factor_list=[fac], date=dt,
                          financial=self.financial, pricing=self.pricing, estimates=self.estimates_cols,
                          data_class_list=self.data_class_list)
        # filter & clean data
        filtered_data = f.filter_all()
        fac_data = CalcManager.get_fac_data(filtered_data=filtered_data, fac=fac)
        return fac_data

    @staticmethod
    def calc_single_fac_single_date(fac: Type[Factor], fac_data_inputs: pd.DataFrame) -> pd.DataFrame:
        # for testing purpose
        return CalcManager.calc_single(fac=fac, fac_data=fac_data_inputs)

    def run_model(self):
        fac_name_list = [fac.name for fac in self.factor_list]
        fac_names = ''.join(str(f) for f in fac_name_list)
        print('Run Model Manager for factor: {facs} \nFrom start date: {s} \nTo end date: {e}'.format(facs=fac_names,
                                                                                                      s=self.start_date,
                                                                                                      e=self.end_date))
        self.values, self.scores, self.factor_data_inputs = self._calculate_all()
        self.__run_status = True
        print('Job done!')


